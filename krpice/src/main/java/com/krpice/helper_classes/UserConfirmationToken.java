package com.krpice.helper_classes;

import com.krpice.domain.User;

/**
 * This is helper class because i need the user confirmation token in DataInitializer class so user can be verified.
 * Antivirus needs to be disabled to send emails.
 */

public class UserConfirmationToken {

    private User user;
    private String confirmationToken;

    public UserConfirmationToken(User user, String confirmationToken) {
        this.user = user;
        this.confirmationToken = confirmationToken;
    }

    public User getUser() {
        return user;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

}
