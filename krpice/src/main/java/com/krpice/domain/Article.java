package com.krpice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.krpice.constants.Category;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
public class Article {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String brand;

    @NotNull
    private int price;

    @NotNull
    private Category category;

    @NotNull
    private Category subcategory;

    @NotNull
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "owner_id", nullable = false)
    @JsonIgnore
    private User owner;

    @ManyToMany(mappedBy = "likedArticles", cascade = CascadeType.ALL)
    private List<User> likes = new LinkedList<>();

    @Column(unique = false)
    private String[] images;

    @ManyToMany
    @JoinTable(name = "hashtags",
                joinColumns = @JoinColumn(name = "article_id"),
                inverseJoinColumns = @JoinColumn(name = "hashtag_id"))
    private Set<Hashtag> hashtags = new HashSet<>();

    public Article() {

    }

    public Article(String title, String brand, int price, String description) {
        this.title = title;
        this.brand = brand;
        this.price = price;
        this.description = description;
        //this.images = new LinkedList<>();
    }

    public Article(String title, String brand, int price, String description, Category category) {
        this(title, brand, price, description);
        this.category = category;
        //this.images = new LinkedList<>();
    }

    public Article(String title, String brand, int price, String description, Category category, Category subcategory, Hashtag... tags) {
        this(title, brand, price, description, category);
        this.subcategory = subcategory;

        for (Hashtag tag : tags) {
            hashtags.add(tag);
        }
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category.toString();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory.toString();
    }

    public void setSubcategory(Category subcategory) {
        this.subcategory = subcategory;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public Long getOwnerId() {
        return owner.getId();
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public int getLikes() {
        return likes.size();
    }

    public void addLike(User u) {
        likes.add(u);
    }

    public Set<Hashtag> getHashtags() {
        return hashtags;
    }

    public void setHashtags(Set<Hashtag> hashtags) {
        this.hashtags = hashtags;
    }

    public void addHashtag(Hashtag hashtag) {
        hashtags.add(hashtag);
    }

}
