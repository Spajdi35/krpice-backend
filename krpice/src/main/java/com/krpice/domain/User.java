package com.krpice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.krpice.constants.City;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
public class User {

    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private Long id;

    @Column(unique = true)
    @NotNull
    private String username;

    @Column(unique = true)
    @NotNull
    //@JsonIgnore
    private String email;

    @NotNull
//    @JsonIgnore
    private String passwordEncoded;

    @NotNull
    private City city;

    private String description;

    // EAGER means to be loaded immediately, opposite of lazy followers etc. below
    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    private List<Article> articles = new LinkedList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "article_like",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "article_id"))
    @JsonIgnore
    private List<Article> likedArticles = new LinkedList<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(joinColumns = @JoinColumn(name = "followed_id"),
            inverseJoinColumns = @JoinColumn(name = "follower_id"))
    @JsonIgnore
    private Set<User> followers = new HashSet<>();

    @ManyToMany(mappedBy = "followers", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<User> following = new HashSet<>();

    private boolean isEnabled = false;

    public User() {

    }

    public User(String email, String username, String password, City city) {
        this.email = email;
        this.username = username;
        this.passwordEncoded = password;
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public City getCity() {
        return city;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public List<Article> getLikedArticles() {
        return likedArticles;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPasswordEncoded() {
        return passwordEncoded;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Set<User> getFollowers() {
        return followers;
    }

    public int getFollowersCount() {
        return followers.size();
    }

    public void setFollowers(Set<User> followers) {
        this.followers = followers;
    }

    public Set<User> getFollowing() {
        return following;
    }

    public int getFollowingCount() {
        return following.size();
    }

    public void setFollowing(Set<User> following) {
        this.following = following;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
}
