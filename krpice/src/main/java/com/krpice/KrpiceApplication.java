package com.krpice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KrpiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KrpiceApplication.class, args);
	}

}