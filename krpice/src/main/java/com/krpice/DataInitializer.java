package com.krpice;

import com.krpice.constants.Category;
import com.krpice.constants.City;
import com.krpice.domain.Article;
import com.krpice.domain.Hashtag;
import com.krpice.domain.User;
import com.krpice.helper_classes.UserConfirmationToken;
import com.krpice.rest.UserRegisterDTO;
import com.krpice.service.ArticleService;
import com.krpice.service.ConfirmAccountService;
import com.krpice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class DataInitializer {

    @Autowired
    private UserService userService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ConfirmAccountService confirmAccountService;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        UserRegisterDTO ownerDTO = new UserRegisterDTO("zeljkohalle@yahoo.com","Lucibela", "malena", City.Zagreb);
        UserRegisterDTO likerDTO = new UserRegisterDTO("lucijahrustic@gmail.com","Zeljko", "mica", City.Zagreb);

        UserRegisterDTO follower1DTO = new UserRegisterDTO("zeljko@clover.studio","Follower1", "disi", City.Zagreb);
        UserRegisterDTO follower2DTO = new UserRegisterDTO("a@gmail.com","Follower2", "jbt", City.Zagreb);
        UserRegisterDTO follower3DTO = new UserRegisterDTO("b@gmail.com","Follower3", "sta ima", City.Zagreb);
        UserRegisterDTO follower4DTO = new UserRegisterDTO("c@gmail.com","Follower5", "sta ima", City.Zagreb);

        UserConfirmationToken owner = userService.register(ownerDTO);
        confirmAccountService.confirmAccount(owner.getConfirmationToken());
        UserConfirmationToken liker = userService.register(likerDTO);
        confirmAccountService.confirmAccount(liker.getConfirmationToken());
        UserConfirmationToken follower1 = userService.register(follower1DTO);
        confirmAccountService.confirmAccount(follower1.getConfirmationToken());
        UserConfirmationToken follower2 = userService.register(follower2DTO);
        confirmAccountService.confirmAccount(follower2.getConfirmationToken());
        UserConfirmationToken follower3 = userService.register(follower3DTO);
        confirmAccountService.confirmAccount(follower3.getConfirmationToken());
        UserConfirmationToken follower4 = userService.register(follower4DTO);
        confirmAccountService.confirmAccount(follower4.getConfirmationToken());

        Article a1 = new Article("Pamucna vesta", "Armani", 2000, "Jako udobna vesta sa cvjetnim detaljima.");
        a1.setCategory(Category.APPAREL);
        a1.setSubcategory(Category.SWEATERS);

        Article a2 = new Article("Remen Burberry", "Burberry", 3500, "Lijepi zimski sal.");
        a2.setCategory(Category.ACCESORIES);
        a2.setSubcategory(Category.BELTS);

        articleService.addArticle(a1, owner.getUser().getId());
        articleService.addArticle(a2, owner.getUser().getId());

        articleService.addArticle("Kozna jakna", "DG", 6500, Category.APPAREL, Category.JACKETS, "Nikad nosena Avirex jakna", owner.getUser().getId(), "manijak", "original", "koza");
        articleService.addArticle("Stikle Louboutin", "Christian Louboutin", 8300, Category.FOOTWEAR, Category.HEELS, "Nove stikle", owner.getUser().getId(), "luksuz", "original", "zmijskakoza");

        userService.followUser(follower1.getUser().getId(), owner.getUser().getId());
        userService.followUser(follower2.getUser().getId(), owner.getUser().getId());
        userService.followUser(follower3.getUser().getId(), liker.getUser().getId());
        userService.followUser(follower3.getUser().getId(), owner.getUser().getId());
    }

}
