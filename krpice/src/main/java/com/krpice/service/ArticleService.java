package com.krpice.service;

import com.krpice.constants.Category;
import com.krpice.domain.Article;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ArticleService {
    List<Article> listAll();

    Article addArticle(Article article, Long ownerId);
    Article addArticle(String title, String brand, int price, Category category, Category subcategory, String description, Long ownerId, String... tags);

    Article uploadImages(Long articleId, MultipartFile... files);
}
