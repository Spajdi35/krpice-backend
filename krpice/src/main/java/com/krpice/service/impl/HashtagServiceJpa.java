package com.krpice.service.impl;

import com.krpice.dao.HashtagRepository;
import com.krpice.domain.Hashtag;
import com.krpice.service.HashtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class HashtagServiceJpa implements HashtagService {

    @Autowired
    private HashtagRepository hashtagRepository;

    @Override
    public Set<Hashtag> listAll() {
        return new HashSet<>(hashtagRepository.findAll());
    }

}
