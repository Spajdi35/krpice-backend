package com.krpice.service.impl;

import com.krpice.dao.ConfirmationTokenRepository;
import com.krpice.dao.UserRepository;
import com.krpice.domain.ConfirmationToken;
import com.krpice.domain.User;
import com.krpice.helper_classes.UserConfirmationToken;
import com.krpice.rest.UserRegisterDTO;
import com.krpice.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceJpa implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }

    @Override
    public UserConfirmationToken register(UserRegisterDTO userRegisterDTO) {
        if (userRepository.countByUsername(userRegisterDTO.getUsername()) > 0) {
            throw new EntityAlreadyExistsException(User.class, userRegisterDTO.getUsername());
        }

        if (userRepository.countByEmail(userRegisterDTO.getEmail()) > 0) {
            throw new EntityAlreadyExistsException(User.class, userRegisterDTO.getEmail());
        }

        String encodedPassword = passwordEncoder.encode(userRegisterDTO.getPassword());
        User newUser = new User(userRegisterDTO.getEmail(), userRegisterDTO.getUsername(), encodedPassword, userRegisterDTO.getCity());

        ConfirmationToken confirmationToken = new ConfirmationToken(newUser);

        confirmationTokenRepository.save(confirmationToken);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(newUser.getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom("sšajdi35@gmail.com");
        mailMessage.setText("To confirm your account, please click here : "
                + "http://localhost:8080/confirmAccount?token=" + confirmationToken.getConfirmationToken());

        emailSenderService.sendEmail(mailMessage);
        userRepository.save(newUser);

        UserConfirmationToken userConfirmationToken = new UserConfirmationToken(newUser, confirmationToken.getConfirmationToken());

        return userConfirmationToken;
    }

    @Override
    public User login(String username, String password) {
        User user = userRepository.findByUsername(username).orElseThrow(
                () -> new EntityMissingException(User.class, username)
        );

        if (!passwordEncoder.matches(password, user.getPasswordEncoded())) {
            throw new WrongPasswordException(password, username);
        }

        return user;
    }

    private User findById(long id) {
        return userRepository.findById(id).orElseThrow(
                () -> new EntityMissingException(User.class, id)
        );
    }

    @Override
    public User followUser(Long followerId, Long toBeFollowedId) {
        User follower = findById(followerId);
        User toBeFollowed = findById(toBeFollowedId);

        follower.getFollowing().add(toBeFollowed);
        toBeFollowed.getFollowers().add(follower);

        userRepository.save(follower);
        userRepository.save(toBeFollowed);

        return follower;
    }

    @Override
    public Set<User> getFollowing(long userId) {
        User u = findById(userId);
        return u.getFollowing();
    }

    @Override
    public Set<User> getFollowers(long userId) {
        User user = findById(userId);
        return user.getFollowers();
    }

    @Override
    public User updateDescription(Long userId, String desc) {
        return userRepository.findById(userId).map(user -> {
            user.setDescription(desc);
            return userRepository.save(user);
        }).orElseThrow(
                () -> new EntityMissingException(User.class, userId)
        );
    }
}
