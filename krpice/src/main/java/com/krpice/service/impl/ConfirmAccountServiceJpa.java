package com.krpice.service.impl;

import com.krpice.dao.ConfirmationTokenRepository;
import com.krpice.dao.UserRepository;
import com.krpice.domain.ConfirmationToken;
import com.krpice.domain.User;
import com.krpice.service.ConfirmAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfirmAccountServiceJpa implements ConfirmAccountService {

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public User confirmAccount(String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if (token != null) {
            User user = userRepository.findByEmailIgnoreCase(token.getUser().getEmail());
            user.setEnabled(true);
            userRepository.save(user);

            return user;
        }

        return null;
    }

}
