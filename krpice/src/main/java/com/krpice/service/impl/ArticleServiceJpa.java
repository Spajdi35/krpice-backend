package com.krpice.service.impl;

import com.krpice.constants.Category;
import com.krpice.dao.ArticleRepository;
import com.krpice.dao.HashtagRepository;
import com.krpice.dao.UserRepository;
import com.krpice.domain.Article;
import com.krpice.domain.Hashtag;
import com.krpice.domain.User;
import com.krpice.service.ArticleService;
import com.krpice.service.EntityMissingException;
import com.krpice.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ArticleServiceJpa implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HashtagRepository hashtagRepository;

    @Override
    public List<Article> listAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article addArticle(Article article, Long ownerId) {
        User owner = userRepository.findById(ownerId).orElseThrow(
                () -> new EntityMissingException(User.class, ownerId)
        );

        article.setOwner(owner);
        owner.getArticles().add(article);

        return articleRepository.save(article);
    }

    @Override
    public Article addArticle(String title, String brand, int price, Category category, Category subcategory, String description, Long ownerId, String... tags) {
        Article article = new Article(title, brand, price, description, category, subcategory);

        for (String tag : tags) {
            Hashtag hashtag;
            if (hashtagRepository.countByName(tag) > 0) {
                hashtag = hashtagRepository.findByName(tag).orElseThrow(
                        () -> new EntityMissingException(Hashtag.class, tag)
                );
            } else {
                hashtag = new Hashtag(tag);
            }

            hashtag.getArticles().add(article);
            article.addHashtag(hashtag);

            hashtagRepository.save(hashtag);
        }

        User owner = userRepository.findById(ownerId).orElseThrow(
                () -> new EntityMissingException(User.class, ownerId)
        );
        owner.getArticles().add(article);
        article.setOwner(owner);

        return articleRepository.save(article);
    }

    @Override
    public Article uploadImages(Long articleId, MultipartFile... files) {
        Article article = articleRepository.findById(articleId).orElseThrow(
                () -> new EntityMissingException(Article.class, articleId)
        );

        String[] fileNames = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            fileNames[i] = files[i].getOriginalFilename();

            System.out.println("Ide file");
            System.out.println(fileNames[i]);
        }

        article.setImages(fileNames);

        Article savedArticle = articleRepository.save(article);
        String uploadDir = "articleImages/" + savedArticle.getId();

        for (int i = 0; i < files.length; i++) {
            try {
                FileUploadUtil.saveFile(uploadDir, "img" + i, files[i]);
            } catch (IOException e) {
                System.out.println("IOException occured!!!!");
            }
        }

        return article;
    }
}
