package com.krpice.service;

import com.krpice.constants.City;
import com.krpice.domain.Article;
import com.krpice.domain.User;
import com.krpice.helper_classes.UserConfirmationToken;
import com.krpice.rest.UserRegisterDTO;

import java.util.List;
import java.util.Set;

public interface UserService {
    List<User> listAll();

    UserConfirmationToken register(UserRegisterDTO userRegisterDTO);
    User login(String username, String password);

    User updateDescription(Long userId, String desc);

    User followUser(Long followerId, Long toBeFollowedId);

    Set<User> getFollowing(long userId);
    Set<User> getFollowers(long userId);
}
