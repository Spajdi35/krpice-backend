package com.krpice.service;

import com.krpice.domain.Hashtag;

import java.util.Set;

public interface HashtagService {
    Set<Hashtag> listAll();
}
