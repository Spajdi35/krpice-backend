package com.krpice.service;

public class EntityAlreadyExistsException extends RuntimeException {

    public EntityAlreadyExistsException(Class<?> cls, Object ref) {
        super("Entity with reference " + ref + " of " + cls + " already exists.");
    }

}
