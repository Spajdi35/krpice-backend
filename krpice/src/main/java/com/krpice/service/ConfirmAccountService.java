package com.krpice.service;

import com.krpice.domain.User;

public interface ConfirmAccountService {
    User confirmAccount(String confirmationToken);
}
