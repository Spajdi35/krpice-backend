package com.krpice.service;

public class WrongPasswordException extends RuntimeException {

    private static final long serialVersionUID = 10L;

    public WrongPasswordException(String password, String username) {
        super("Password: " + password + " is false for user " + username);
    }

}
