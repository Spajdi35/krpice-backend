package com.krpice.dao;

import com.krpice.domain.Hashtag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HashtagRepository extends JpaRepository<Hashtag, Long> {
    int countByName(String name);
    Optional<Hashtag> findByName(String name);
}
