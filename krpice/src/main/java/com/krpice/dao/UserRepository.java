package com.krpice.dao;

import com.krpice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    int countByUsername(String username);
    int countByEmail(String email);
    Optional<User> findByUsername(String username);
    User findByEmailIgnoreCase(String email);
}
