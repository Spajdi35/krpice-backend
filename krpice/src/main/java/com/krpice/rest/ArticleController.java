package com.krpice.rest;

import com.krpice.domain.Article;
import com.krpice.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping("")
    public List<Article> listAll() {
        return articleService.listAll();
    }

    @PostMapping("/addArticle/{ownerId}")
    public Article addArticle(@PathVariable("ownerId") Long ownerId, @RequestBody AddArticleDTO addArticleDTO) {
        return articleService.addArticle(addArticleDTO.getTitle(), addArticleDTO.getBrand(), addArticleDTO.getPrice(), addArticleDTO.getCategory(),
                addArticleDTO.getSubcategory(), addArticleDTO.getDescription(), ownerId, addArticleDTO.getHashtags());
    }

    @PostMapping(value = "/{articleId}/upload_multiple", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Article handleFormSubmit(@PathVariable("articleId") Long articleId, @RequestPart(value = "images", required = true) MultipartFile... files) throws IOException {
        return articleService.uploadImages(articleId, files);
    }

}
