package com.krpice.rest;

import com.krpice.dao.ConfirmationTokenRepository;
import com.krpice.dao.UserRepository;
import com.krpice.domain.ConfirmationToken;
import com.krpice.domain.User;
import com.krpice.helper_classes.UserConfirmationToken;
import com.krpice.service.ConfirmAccountService;
import com.krpice.service.EmailSenderService;
import com.krpice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ConfirmAccountService confirmAccountService;

    @Autowired
    private EmailSenderService emailSenderService;

    @GetMapping("")
    public List<User> listUsers() {
        return userService.listAll();
    }

    @PostMapping("/register")
    public UserConfirmationToken registerUser(@RequestBody UserRegisterDTO userRegisterDTO)  {
        return userService.register(userRegisterDTO);
    }

    @PostMapping("/confirmAccount")
    public User confirmUserAccount(@RequestParam("token") String confirmationToken) {
       return confirmAccountService.confirmAccount(confirmationToken);
    }

    @PostMapping("/login")
    public User login(@RequestBody UserLoginDTO userLoginDTO) {
        return userService.login(userLoginDTO.getUsername(), userLoginDTO.getPassword());
    }

    @GetMapping("/{id}/following")
    public Set<User> getFollowing(@PathVariable("id") long id) {
        return userService.getFollowing(id);
    }

    @GetMapping("/{id}/followers")
    public Set<User> getFollowers(@PathVariable("id") long id) {
        return userService.getFollowers(id);
    }

    @GetMapping("/getFromFacebook")
    public Principal getUserFromFacebook(Principal user) {
        return user;
    }

    @PutMapping("/{id}/updateDescription")
    public User updateDescription(@PathVariable("id") Long userId, @RequestBody String desc) {
        return userService.updateDescription(userId, desc);
    }

}
