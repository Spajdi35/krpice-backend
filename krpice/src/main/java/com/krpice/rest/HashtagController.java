package com.krpice.rest;

import com.krpice.domain.Hashtag;
import com.krpice.service.HashtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/hashtags")
public class HashtagController {

    @Autowired
    private HashtagService hashtagService;

    @GetMapping("")
    public Set<Hashtag> listAll() {
        return hashtagService.listAll();
    }

}
