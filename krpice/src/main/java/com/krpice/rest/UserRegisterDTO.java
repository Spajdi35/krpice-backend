package com.krpice.rest;

import com.krpice.constants.City;
import com.krpice.domain.User;

public class UserRegisterDTO {

    private String email;
    private String username;
    private String password;
    private City city;

    public UserRegisterDTO(String email, String username, String password, City city){
        this.email = email;
        this.username = username;
        this.password = password;
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public User toUser() {
        return new User(getEmail(), getUsername(), getPassword(), getCity());
    }

}
