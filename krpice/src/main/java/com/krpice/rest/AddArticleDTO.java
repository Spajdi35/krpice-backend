package com.krpice.rest;

import com.krpice.constants.Category;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AddArticleDTO {

    private String title;
    private String brand;
    private int price;
    private Category category;
    private Category subcategory;
    private String description;
    private String[] hashtags;

    public AddArticleDTO(String title, String brand, int price, Category category, Category subcategory, String description, String... hashtags) {
        this.title = title;
        this.brand = brand;
        this.price = price;
        this.category = category;
        this.subcategory = subcategory;
        this.description = description;
        this.hashtags = hashtags;
    }

    public String getTitle() {
        return title;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }

    public Category getCategory() {
        return category;
    }

    public Category getSubcategory() {
        return subcategory;
    }

    public String getDescription() {
        return description;
    }

    public String[] getHashtags() {
        return hashtags;
    }

}
