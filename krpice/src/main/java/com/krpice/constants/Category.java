package com.krpice.constants;

public enum Category {

    APPAREL("Odjeća"), FOOTWEAR("Obuća"), ACCESORIES("Accesories"), KIDS_WEAR("Dječja odjeća"),  OTHERS("Ostalo"),

    WALLETS("Novčanici"), BELTS("Remeni"), UNDERWEAR("Donje rublje"), JACKETS("Jakne"),
     BAGS("Torbe"), HEELS("Štikle"), TRAINERS("Tenisice"), BRAS("Grudnjaci"), SWEATERS("Veste"),
    JEANS("Traperice"), DRESSES("Haljine"), SHIRTS("Košulje"), TSHIRTS("Majice"), BOOTS("Čizme");

    private String category;

    Category(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return category;
    }
}
