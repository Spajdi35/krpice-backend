package com.krpice.mail_sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(String recipientEmail, String text) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(recipientEmail);

        msg.setSubject("Testing from Spring Boot");
        msg.setText(text);

        javaMailSender.send(msg);
    }

}
