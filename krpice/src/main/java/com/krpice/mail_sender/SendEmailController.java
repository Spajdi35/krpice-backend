package com.krpice.mail_sender;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class SendEmailController {

    @Autowired
    private EmailSenderService emailSenderService;

    // for sending the mail:
    // 1. antivirus should be disabled
    // 2. 2 step auth should be disabled on gmail from where mail is sent
    // 3. you should allow less secure apps on gmail
    @RequestMapping(value = "/sendEmail")
    public String send() throws AddressException, MessagingException, IOException {
        emailSenderService.sendEmail("lucijahrustic@gmail.com", "Desi malicka sta mai? Malo za obleku testiram slanje mailova");
        return "Email sent successfully";
    }
}
