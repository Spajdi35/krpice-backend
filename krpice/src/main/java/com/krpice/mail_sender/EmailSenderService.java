package com.krpice.mail_sender;

public interface EmailSenderService {
    public void sendEmail(String recipientEmail, String text);
}
